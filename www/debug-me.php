<?php

/******* INSTRUCTIONS *****************
 * no lines of code should be added
 * and none should be removed
 * but all lines of code may be edited
 * and any may be moved
 *************************************/

function make_User($user_name){
$new_user = [];  
$new_user["name"] = $user_name;
return $new_user["name"];
}

$users[] = make_User('William "Refrigerator" Perry');
$users[] = make_User("Gargamel");
$users[] = make_User("Chris O'Dowd");

?><!DOCTYPE html>
<html>
  <body>
    <ul>
      <?php foreach($users as $person){ ?>
        <li><?= $person ?></li>
      <?php } ?>
    </ul>
  </body>
</html>
